# Instalação

Baixar o repositório
Abrir a a pasta do repositório e executar:  
`docker-compose up`


# Considerações Importantes

* Como fiquei na duvida de como fazer as 3 task, eu fiz 1 chamada que executa as 3 task gerando um html no navegador e como RestApi.
* Elasticsearch não foi usado por 2 motivos:
 - Primeiro que eu não tenho experiencia com o uso dele
 - Segundo que seria necessário um tempo para dar uma estuda e não tenho esse tempo livre.
* Vagrant com VirtualBox não foi feito pelo mesmo motivo do Elasticsearch, é possível rodar o projeto local com o docker-compose, se por alguma
instancia só liberar a porta que foi estabelecida pelo desafio 8080, e garantir que tenha o docker instalado.
* Caso seja necessário subir uma infra eu gero um terraform que sobe a instância na AWS.


# Usando 

* No navegador ao abrir o localhost:8080 que foi definido no compose, sera possível ver o resultado das 3 task em um html gerado pelo ejs.
* Usando como RestApi só fazer as chamadas: localhost:8080/task1
* Usando como RestApi só fazer as chamadas: localhost:8080/task2
* Usando como RestApi só fazer as chamadas: localhost:8080/task3

# Test

* Apenas alguns testes foram executados, só pra mostrar como eu faço alguns.