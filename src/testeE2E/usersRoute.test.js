const request = require('supertest')
const app = require('../app')
const expect = require('chai').expect

describe('Testing RestAPI E2E', () => {
  it('task1', done => {
    request(app)
      .get('/task1')
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        expect(res.body.length).to.be.equals(10)
        expect(res.body[0].website).to.be.equals('hildegard.org')
        done()
      })
  })
})