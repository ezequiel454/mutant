const usersController = require ('./users')
const expect = require('chai').expect
const sinon = require('sinon')

describe('Users Controller', () => {
  it('user to obj', () => {
    let lista = [
      {
        "id": 1,
        "name": "Leanne Graham",
        "username": "Bret",
        "email": "Sincere@april.biz",
        "address": {
          "street": "Kulas Light",
          "suite": "Apt. 556",
          "city": "Gwenborough",
          "zipcode": "92998-3874",
          "geo": {
            "lat": "-37.3159",
            "lng": "81.1496"
          }
        },
        "phone": "1-770-736-8031 x56442",
        "website": "hildegard.org",
        "company": {
          "name": "Romaguera-Crona",
          "catchPhrase": "Multi-layered client-server neural-net",
          "bs": "harness real-time e-markets"
        }
      },
    ]
    const result = usersController.userToObj(lista[0])
    expect("Leanne Graham").to.equals(result.name)
    expect(4).to.equals(Object.getOwnPropertyNames(result).length)
  })
  it('Order by name', () => {
    let lista = [
      {
        "name": "Leanne Graham"
      },
      {
        "name": "Amanda Lima"
      },
      {
        "name": "Ana Clara"
      },
      {
        "name" : "bruno João"
      }
    ]
    const result = lista.sort((a, b) =>  usersController.sortByName(a.name.toLowerCase(), b.name.toLowerCase()))
    expect("Amanda Lima").to.equals(result[0].name)
    expect("Ana Clara").to.equals(result[1].name)
    expect("bruno João").to.equals(result[2].name)
    expect("Leanne Graham").to.equals(result[3].name)
  })
  it('Task1', () => {
    let lista = { 
      "data": [
        {
          "id": 1,
          "name": "Leanne Graham",
          "username": "Bret",
          "email": "Sincere@april.biz",
          "address": {
            "street": "Kulas Light",
            "suite": "Apt. 556",
            "city": "Gwenborough",
            "zipcode": "92998-3874",
            "geo": {
              "lat": "-37.3159",
              "lng": "81.1496"
            }
          },
          "phone": "1-770-736-8031 x56442",
          "website": "hildegard.org",
          "company": {
            "name": "Romaguera-Crona",
            "catchPhrase": "Multi-layered client-server neural-net",
            "bs": "harness real-time e-markets"
          }
        },
        {
          "id": 2,
          "name": "Ervin Howell",
          "username": "Antonette",
          "email": "Shanna@melissa.tv",
          "address": {
            "street": "Victor Plains",
            "suite": "Suite 879",
            "city": "Wisokyburgh",
            "zipcode": "90566-7771",
            "geo": {
              "lat": "-43.9509",
              "lng": "-34.4618"
            }
          },
          "phone": "010-692-6593 x09125",
          "website": "anastasia.net",
          "company": {
            "name": "Deckow-Crist",
            "catchPhrase": "Proactive didactic contingency",
            "bs": "synergize scalable supply-chains"
          }
        },
      ]
    }
    let res = {
      send: function(){}
    }
    let req = { }
    let mock = sinon.mock(res)
    mock.expects('send').once().withArgs([{  "website": "hildegard.org"  }, { "website": "anastasia.net" }])
    usersController.task1(lista, req, res)
  })
})