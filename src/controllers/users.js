const axios = require('axios');

const userToObj = (obj, cond = true) => {
    const user = {
        name: obj.name,
        email: obj.email,
        company: obj.company.name,
    }
    if(cond){
        user.website = obj.website
    }
    return user
}

const sortByName = (a, b) => (a < b) ? -1 : (a > b) ? 1 : 0

const getApi = async() =>  await axios.get('https://jsonplaceholder.typicode.com/users')

const anyApi = async(usersJson) => {
    let results = []
    if(usersJson !== null && usersJson.length !== 0){
        results = usersJson
    }else{
        results = await getApi()
    }
    return results
}

const findAll = async (usersJson, req, res) => {
    let results = await anyApi(usersJson)
    results = results.data
        .filter(obj => obj.address.suite.toLowerCase().includes('suite'))
        .map(obj => userToObj(obj))
        .sort((a, b) =>  sortByName(a.name.toLowerCase(), b.name.toLowerCase()))
    res.render('index', { users: results })
}

const task1 = async (usersJson, req, res) => {
    let results = await anyApi(usersJson)
    res.statusCode = 200
    res.send( results.data.map(p => ({website: p.website}))) 
}

const task2 = async (usersJson, req, res) => {
    let results = await anyApi(usersJson)
    res.statusCode = 200
    res.send( results.data
        .map(obj => userToObj(obj, false))
        .sort((a, b) =>  sortByName(a.name.toLowerCase(), b.name.toLowerCase())) )
}

const task3 = async (usersJons, req, res) => { 
    let results = await anyApi(usersJson)
    res.statusCode = 200
    res.send( results.data.filter(obj => obj.address.suite.toLowerCase().includes('suite')) ) 
}

module.exports = {
    findAll, task1, task2, task3, userToObj, sortByName
}
