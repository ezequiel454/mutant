const express = require('express')
const users = require('../controllers/users')
const axios = require('axios');

const router = express.Router()

let usersJson = []

const usersToJson = async() => {
        router.get('/', users.findAll.bind(null, usersJson))
        router.get('/task1', users.task1.bind(null, usersJson))
        router.get('/task2', users.task2.bind(null, usersJson))
        router.get('/task3', users.task3.bind(null, usersJson))
} 

usersToJson()

module.exports = router
