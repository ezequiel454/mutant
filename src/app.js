const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const routes = require('./routes/users')
const path = require('path')

const app = express()

app.use(cors())
app.use(bodyParser.json({ extended: true }))
app.use(routes)

app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')

module.exports = app